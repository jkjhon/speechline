package com.aim.quizapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;

import com.aim.quizapp.adapter.AnswerViewAdapter;
import com.aim.quizapp.model.Question;

import java.util.ArrayList;

public class AnswerActivity extends AppCompatActivity {

    ArrayList<Question> list = new ArrayList<Question>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_answer);

        Intent intent = getIntent();

        list = intent.getParcelableArrayListExtra(QuestionActivity.QLIST);

        final RecyclerView recyclerView;

        final RecyclerView.Adapter[] adapter = new RecyclerView.Adapter[1];

        recyclerView = findViewById(R.id.recycleViewAns);

        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(AnswerActivity.this));

        recyclerView.setAdapter(adapter[0]);

        adapter[0] = new AnswerViewAdapter(AnswerActivity.this, list);

        recyclerView.setAdapter(adapter[0]);
    }
}
