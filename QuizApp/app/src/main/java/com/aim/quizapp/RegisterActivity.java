package com.aim.quizapp;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class RegisterActivity extends AppCompatActivity {

    TextView tvLog;
    Button btnReg;
    EditText etName,etEmail,etPwd;
    DatabaseReference databaseReference;
    FirebaseAuth firebaseAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        etName = findViewById(R.id.etName);
        etPwd = findViewById(R.id.etPwd);
        etEmail = findViewById(R.id.etEmail);
        btnReg = findViewById(R.id.btn);
        tvLog = findViewById(R.id.tv);

        //--- Login TextView -----------
        tvLog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);
                startActivity(intent);
            }
        });

        //--------- loading dialog -----------------
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(R.layout.progress_dialog);
        final Dialog dialog = builder.create();

        databaseReference = FirebaseDatabase.getInstance().getReference("User");
        firebaseAuth = FirebaseAuth.getInstance();

        //--- Register Button -----------
        btnReg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.show();
                final String name = etName.getText().toString();
                final String email = etEmail.getText().toString();
                String pwd = etPwd.getText().toString();

                if (name.isEmpty()){
                    etName.setError("Please enter name");
                    etName.requestFocus();
                }else if (email.isEmpty()){
                    etEmail.setError("Please enter email");
                    etEmail.requestFocus();
                }else if(pwd.isEmpty()){
                    etPwd.setError("Please enter password");
                    etPwd.requestFocus();
                }else if(!name.isEmpty() && !email.isEmpty() && !pwd.isEmpty()) {
                    firebaseAuth.createUserWithEmailAndPassword(email,pwd)
                            .addOnCompleteListener(RegisterActivity.this, new OnCompleteListener<AuthResult>() {

                                @Override
                                public void onComplete(@NonNull Task<AuthResult> task) {
                                    if (task.isSuccessful()) {
                                        FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();
                                        UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder().setDisplayName(name).build();
                                        user.updateProfile(profileUpdates);
                                        dialog.dismiss();
                                        Toast.makeText(RegisterActivity.this, "Registration complete.", Toast.LENGTH_SHORT).show();
                                        Intent intent = new Intent(RegisterActivity.this,LoginActivity.class);
                                        startActivity(intent);
                                    } else {
                                        dialog.dismiss();
                                        Toast.makeText(RegisterActivity.this, " Registration Fail", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                }
            }
        });

    }

}
