package com.aim.quizapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.aim.quizapp.model.Question;
import com.aim.quizapp.model.Statistics;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class ResultActivity extends AppCompatActivity {

    int pStatus = 0;
    private Handler handler = new Handler();
    TextView tvP,tvCorrect,tvWrong,tvSkip,tvComment;
    public static final String QLIST = "com.aim.quizapp.QLIST";
    ArrayList<Question> list = new ArrayList<Question>();
    int flag = 0;
    DatabaseReference reference;
    Statistics stat;
    private FirebaseUser firebaseUser;
    private InterstitialAd mInterstitialAd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result);

        //-------ad initialize-----------------
        MobileAds.initialize(this, "ca-app-pub-3940256099942544~3347511713");
        mInterstitialAd = new InterstitialAd(this);
        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");
        mInterstitialAd.loadAd(new AdRequest.Builder().build());

        tvCorrect = findViewById(R.id.tvCorrect);
        tvWrong = findViewById(R.id.tvWrong);
        tvSkip = findViewById(R.id.tvSkip);
        tvP = findViewById(R.id.tvPercentage);
        tvComment = findViewById(R.id.tvComment);

        Intent intent = getIntent();
        list = intent.getParcelableArrayListExtra(QuestionActivity.QLIST);
        String cat = intent.getStringExtra(QuestionActivity.CATEGORY);
        String cr = intent.getStringExtra(QuestionActivity.CORRECT);
        String wr = intent.getStringExtra(QuestionActivity.WRONG);
        String tot = intent.getStringExtra(QuestionActivity.TOTAL);

        int correct = Integer.parseInt(cr);
        int wrong = Integer.parseInt(wr);
        int total = Integer.parseInt(tot);
        int skip = total-correct-wrong;

        final int p = (correct*100)/total;

        tvCorrect.setText(cr);
        tvWrong.setText(wr);
        tvSkip.setText(String.valueOf(skip));

        Resources res = getResources();
        Drawable drawable = res.getDrawable(R.drawable.circular);
        final ProgressBar mProgress = (ProgressBar) findViewById(R.id.circularProgressbar);
        mProgress.setProgress(0);   // Main Progress
        mProgress.setSecondaryProgress(100); // Secondary Progress
        mProgress.setMax(100); // Maximum Progress
        mProgress.setProgressDrawable(drawable);

        new Thread(new Runnable() {

            @Override
            public void run() {
                // TODO Auto-generated method stub
                while (pStatus<p) {
                    pStatus += 1;

                    handler.post(new Runnable() {

                        @Override
                        public void run() {
                            // TODO Auto-generated method stub
                            mProgress.setProgress(pStatus);
                            tvP.setText(pStatus + "%");
                        }
                    });
                    try {
                        Thread.sleep(8);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

        if(p >= 80) {
            tvComment.setText(R.string.tx_best);
        }else if(p >= 50) {
            tvComment.setText(R.string.tx_good);
        }else{
            tvComment.setText(R.string.tx_bad);
        }

        //------------------------ Store score--------------------------------------------------
        if(flag == 0) {
            firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
            reference = FirebaseDatabase.getInstance().getReference().child("Statistics").child(firebaseUser.getUid());
            stat = new Statistics();
            stat.setCorrect(correct);
            stat.setWrong(wrong);
            stat.setScore(p);
            stat.setSkip(skip);
            stat.setCategory(cat);

            String date = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault()).format(new Date());
            stat.setDate(date);

            reference.push().setValue(stat);

            flag++;
        }

        //------------------------ad click listner -------------------
        mInterstitialAd.setAdListener(new AdListener() {
            @Override
            public void onAdLoaded() {
                // Code to be executed when an ad finishes loading.
            }

            @Override
            public void onAdFailedToLoad(int errorCode) {
                // Code to be executed when an ad request fails.
            }

            @Override
            public void onAdOpened() {
                // Code to be executed when the ad is displayed.
            }

            @Override
            public void onAdClicked() {
                // Code to be executed when the user clicks on an ad.
            }

            @Override
            public void onAdLeftApplication() {
                // Code to be executed when the user has left the app.
            }

            @Override
            public void onAdClosed() {
                // Code to be executed when the interstitial ad is closed.
                Intent intent = new Intent(ResultActivity.this,AnswerActivity.class);
                intent.putParcelableArrayListExtra(QLIST, (ArrayList<Question>) list);
                startActivity(intent);
            }
        });



    }

    // --- view answer button -------------------------------------------------------------------
    public void answerBtn(View view){
        if (mInterstitialAd.isLoaded()) {
            mInterstitialAd.show();
        } else {
            Intent intent = new Intent(ResultActivity.this,AnswerActivity.class);
            intent.putParcelableArrayListExtra(QLIST, (ArrayList<Question>) list);
            startActivity(intent);
        }

    }

    //--- retake button -------------------------------------------------------------------------
    public void retakeBtn(View view){
        startActivity(new Intent(this,MainActivity.class));
    }

    @Override
    public void onBackPressed() {

    }
}
