package com.aim.quizapp;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.aim.quizapp.adapter.ScoreViewAdapter;
import com.aim.quizapp.model.Statistics;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;


public class ScoreFragment extends Fragment {


    private FirebaseUser firebaseUser;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(R.string.menu_score);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_score, container, false);

        DatabaseReference databaseReference;

        final List<Statistics> list = new ArrayList<>();

        final RecyclerView recyclerView;

        final RecyclerView.Adapter[] adapter = new RecyclerView.Adapter[1];

        recyclerView = view.findViewById(R.id.recyclerViewS);

        recyclerView.setHasFixedSize(true);

        recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));

        recyclerView.setAdapter(adapter[0]);

        adapter[0] = new ScoreViewAdapter(ScoreFragment.this, list);

        recyclerView.setAdapter(adapter[0]);

        firebaseUser = FirebaseAuth.getInstance().getCurrentUser();

        databaseReference = FirebaseDatabase.getInstance().getReference().child("Statistics").child(firebaseUser.getUid());

        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot snapshot) {

                for (DataSnapshot dataSnapshot : snapshot.getChildren()) {

                    Statistics st = dataSnapshot.getValue(Statistics.class);

                    list.add(st);
                }

                adapter[0] = new ScoreViewAdapter(ScoreFragment.this, list);

                recyclerView.setAdapter(adapter[0]);

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

        return view;
    }



}
