package com.aim.quizapp;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.android.gms.ads.initialization.InitializationStatus;
import com.google.android.gms.ads.initialization.OnInitializationCompleteListener;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class SettingsFragment extends Fragment {

    private AdView mAdView;
    String list[] = new String[]{"Clear Records","Delete Account","Privacy Policy","Terms and Conditions"};
    private FirebaseUser firebaseUser;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().setTitle(R.string.menu_settings);

        MobileAds.initialize(getContext(), new OnInitializationCompleteListener() {
            @Override
            public void onInitializationComplete(InitializationStatus initializationStatus) {
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);

        ListView listView = view.findViewById(R.id.listView);
        ArrayAdapter adapter = new ArrayAdapter(getContext(),R.layout.item_settings,list);
        listView.setAdapter(adapter);

        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setView(R.layout.progress_dialog);
        Dialog dialog = builder.create();

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                if (position == 0){
                    firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
                    DatabaseReference dbNode = FirebaseDatabase.getInstance().getReference().getRoot().child("Statistics").child(firebaseUser.getUid());
                    dbNode.setValue(null);
                    Toast.makeText(getContext(), "Records Cleared Successfully", Toast.LENGTH_SHORT).show();
                }else if (position == 1){
                    AlertDialog.Builder alert = new AlertDialog.Builder(getContext());
                    final EditText edittext = new EditText(getContext());
                    edittext.setTransformationMethod(PasswordTransformationMethod.getInstance());
                    alert.setMessage("Enter your password to delete account");
                    alert.setTitle("Enter Your Password");

                    alert.setView(edittext);

                    alert.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            setDialog(true);
                            String pwd = edittext.getText().toString();
                            firebaseUser = FirebaseAuth.getInstance().getCurrentUser();
                            DatabaseReference dbNode = FirebaseDatabase.getInstance().getReference().getRoot().child("Statistics").child(firebaseUser.getUid());
                            dbNode.setValue(null);
                            AuthCredential credential = EmailAuthProvider.getCredential(firebaseUser.getEmail(), pwd);
                            firebaseUser.reauthenticate(credential)
                                    .addOnCompleteListener(new OnCompleteListener<Void>() {
                                        @Override
                                        public void onComplete(@NonNull Task<Void> task) {
                                            firebaseUser.delete().addOnCompleteListener(new OnCompleteListener<Void>() {
                                                @Override
                                                public void onComplete(@NonNull Task<Void> task) {
                                                    if (task.isSuccessful()) {
                                                        setDialog(false);
                                                        startActivity(new Intent(getContext(),LoginActivity.class));
                                                    } else {
                                                        setDialog(false);
                                                        startActivity(new Intent(getContext(),LoginActivity.class));
                                                    }
                                                }
                                            });
                                        }
                                    });
                        }
                    });

                    alert.setNegativeButton("CANCEL", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            // what ever you want to do with No option.
                        }
                    });
                    alert.show();

                }else if(position == 2){
                    // --- add privacy policy---
                }else if(position == 3){
                    // --- add terms and condition ----
                }
            }
        });

        mAdView = view.findViewById(R.id.adView2);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);


        return view;
    }

    private void setDialog(boolean show){
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        //View view = getLayoutInflater().inflate(R.layout.progress);
        builder.setView(R.layout.progress_dialog);
        Dialog dialog = builder.create();
        if (show)
            dialog.show();
        else
            dialog.dismiss();
    }


}
