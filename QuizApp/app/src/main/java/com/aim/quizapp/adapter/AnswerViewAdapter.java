package com.aim.quizapp.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.aim.quizapp.AnswerActivity;
import com.aim.quizapp.R;
import com.aim.quizapp.model.Question;

import java.util.List;

public class AnswerViewAdapter extends RecyclerView.Adapter<AnswerViewAdapter.ViewHolder> {

    AnswerActivity context;
    List<Question> questionList;

    public AnswerViewAdapter(AnswerActivity context, List<Question> TempList) {
        this.questionList = TempList;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_answer, parent, false);

        ViewHolder viewHolder = new ViewHolder(view);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Question qus= questionList.get(position);

        holder.tvQno.setText(String.valueOf(position+1));
        holder.tvQuestion.setText(qus.getQuestion());
        holder.tvCAnswer.setText(qus.getAnswer());
        holder.tvChoice.setText(qus.getChoice());


        if(qus.getSt().equals("c")){
            holder.img.setImageResource(R.drawable.icon_correct);
        }

        if(qus.getSt().equals("s")){
            holder.img.setImageResource(R.drawable.icon_skip);
        }

    }

    @Override
    public int getItemCount() {
        return questionList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        public TextView tvQuestion;
        public TextView tvCAnswer;
        public TextView tvChoice;
        public TextView tvQno;
        public ImageView img;

        public ViewHolder(View itemView) {
            super(itemView);

            tvQuestion = itemView.findViewById(R.id.tvQus);
            tvCAnswer = itemView.findViewById(R.id.tvCans);
            tvChoice = itemView.findViewById(R.id.tvYans);
            tvQno = itemView.findViewById(R.id.tvQCount);
            img = itemView.findViewById(R.id.imgCRK);
        }
    }

}
